# SSO Sigap OAuth2-Client package for Laravel
## Introdução
Biblioteca para realização do **login no SSO do prodap**.

## Antes de instalar:

Você deve cadastrar sua aplicação no [Sistema de SSO (sigp)](https://ssohomo.prodap.ap.gov.br) para gerar os dados de:  **Client ID** e **Cliente SECRET**, da sua aplicação para usar o **SSO**.

## Instalação no Laravel 6.x

No arquivo de `composer.json` adicione as linhas:

```json
...,
"repositories": [{
    "type":"vcs",
    "url": "https://gitlab.com/govdigitalprodap/LaravelSSOClient"
}],
```

Para indicar o repositório do projeto como fonte de busca pelo **composer** e adicione o `"Prodap/LaravelSSO": "^1"` Como dependência do projeto.
```json
...,
"require": {
    ...
    "prodap/laravel-sso-login": "^1"
},
```
No terminal execute o **composer update** para instalar o pacote na sua aplicação:

```sh
     $ composer update
 ```

## Configuração:

Adicione suas **Variáveis de Ambientes** ou no arquivo `.env` as seguintes variáveis:
**SIGP_BASE_URL**, **SIGP_CLIENT_ID**, **SIGP_CLIENT_SECRET** e **SIGP_CALLBACK_URL**:
```php
SIGP_BASE_URL = # URL do sistema de SSO
SIGP_CLIENT_ID =  # O Client Id da sua aplicação cadastrado no SSO
SIGP_CLIENT_SECRET =# O Cliente SECRET cadastrado da sua aplicação cadastrado no SSO
SIGP_CALLBACK_URL =  # URL com a para rota de respostas depois de logado no SSO
```
E no arquivo `/config/services.php`, adicione os dados do serviço para o **sigp**:
```php
...,
'sigp'  => [
    'base_url'  =>  env('SIGP_BASE_URL'),
    'client_id'  =>  env('SIGP_CLIENT_ID'),
    'client_secret'  =>  env('SIGP_CLIENT_SECRET'),
    'redirect'  =>  env('SIGP_CALLBACK_URL'),
]
  ```
Pronto agora você pode Utilizar a facade `SSOSigp` da biblioteca em seu projeto! :v:

## Utilização:

No seu arquivo `routers/web.php` você precisará ter ao pelo menos **3 rotas**:  **Uma para redirecionar a aplicação para o o SSO**;  **Uma para Receber os dados após a autenticação** e por último **Uma para logout no SSO.**

### Exemplo apontando para `LoginController.php`:
```php
Route::get('/login', 'Auth\LoginController@redirectToProvider')->name('login');
Route::get('login/sucesso', 'Auth\LoginController@handleProviderCallback');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');
 ```
Em `app/Http/Controllers/Auth/LoginController.php` utilize os métodos para receber cada ação.

### Exemplo:
```php
<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use SSOSigp; // Faça a chamada da Facade SSOSigp

class LoginController extends Controller
{
...
    /*
    * Ação para redirecionar para tela de login do SSO.
    */
    public function redirectToProvider()
    {
        //Pode incluir a lógica da sua aplicação antes de redirecionar para o SSO.
        return  SSOSigp::driver('sigp')->redirect();//Metodo para redirecionar para o SSO
    }

    /*
    * Ação para receber o callback do SSO após a autenticação do usuário.
    */
    public function handleProviderCallback()
    {
        $user =  SSOSigp::driver('sigp')->user();// Você pode usar o método user() para obter os dados do usuário retornado pelo SSO.

        /*
        * Restante da lógica da sua aplicação ao receber esses dados.
        * Por exemplo:
        * Salvar os dados como o token do usuário na sessão para recuperar posteriormente e fazer login na própria aplicação local.
        */

    }

    /*
    * {@inheritdoc}
    * Ação para executar o logout do SSO.
    */
    public function logout(Request $request)
    {
        /*
        * Pode aplicar a lógica da sua aplicação antes de executar o logout do SSO.
        * Por exemplo:
        * Recuperar o token do usuário logado para a variável $token
        */

        $sso =  SSOSigp::driver('sigp');
        $sso->revokeToken($token); // Método revokeToken() rececebe o Token do usuário logado para revogá-lo

        /*
        * Pode aplicar a lógica da sua aplicação antes de sair das aplicações gerenciadas pelo SSO.
        * Por exemplo:
        * Fazer o logout na aplicação local e apagar a sessão com os dados do usuário.
        */

        return $sso->logout(); // Método logout() do SSOSigp realiza o logout do usuário em todas as aplicações gerenciadas pelo SSO.
    }
}
```

>  Lembre-se de sempre usar `SSOSigp::driver('sigp')` passando o driver `'sigp'` pois futuramente podem ser criado outros drivers.

## Extras e dicas:

Você pode utilizar o Método:
``` php
SSOSigp::driver('sigp')->checkToken($token)['logado']
```
Para receber o **status booleano** se o **token** do usuário ainda é **válido**.

Isso pode ser adicionado ao um **Middleware** para checar as rotas constantemente se o usuário ainda está logado no **SSO**.

### Exemplo de Middleware `app/Http/Middleware/CheckTokenSOO.php`:
``` php
<?php

namespace App\Http\Middleware;

use Closure;
use SSOSigp;
class CheckTokenSOO
{
    /*
    * Handle an incoming request.
    *
    * @param \Illuminate\Http\Request $request
    * @param \Closure $next
    * @return  mixed
    */
    public function handle($request, Closure $next)
    {
        /*
        * Pode aplicar a lógica da sua aplicação antes de executar o logout do SSO.
        * Por exemplo:
        * Recuperar o token do usuário logado para a variável $token
        */
        $sso =  SSOSigp::driver('sigp');
        if(!$sso->checkToken($token)['logado']) {

            /*
            * Pode aplicar a lógica da sua aplicação antes de sair totalmente das aplicações gerenciadas pelo SSO.
            * Por exemplo:
            * Fazer o logout na aplicação local e apagar a sessão com os dados do usuário.
            */

            return $sso->logout(); //Realizar o logout caso o token não seja válido
        }
        return $next($request);
    }
}
```