<?php

namespace Prodap\LaravelSSO;

use Illuminate\Support\ServiceProvider;
use Prodap\LaravelSSO\Contracts\Factory;
use Prodap\LaravelSSO\SSOManager;

class SSOServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(Factory::class, function ($app) {
            return new SSOManager($app);
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [Factory::class];
    }
}
