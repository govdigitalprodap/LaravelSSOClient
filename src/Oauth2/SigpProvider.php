<?php

namespace Prodap\LaravelSSO\Oauth2;

use GuzzleHttp\ClientInterface;

class SigpProvider extends AbstractProvider implements ProviderInterface
{
    /**
     * {@inheritdoc}
     */
    protected function getAuthUrl($state)
    {
        return $this->buildAuthUrlFromBase($this->baseUrl .'sso', $state);
    }

    /**
     * {@inheritdoc}
     */
    protected function getTokenUrl()
    {
        return $this->baseUrl . 'api/validar/';
    }

    /**
     * {@inheritdoc}
     */
    protected function getUserByToken($token)
    {
        $response = $this->getHttpClient()->get($this->baseUrl . 'api/admin/profile/', [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer '.$token,
            ],
        ]);
        return json_decode($response->getBody(), true);
    }

    public function checkToken($token)
    {
        $postKey = (version_compare(ClientInterface::VERSION, '6') === 1) ? 'form_params' : 'body';

        $response = $this->getHttpClient()->post($this->baseUrl . 'api/checar/', [
            'headers' => ['Accept' => 'application/json'],
            $postKey => ['access_token' => $token],
            ]);
        return json_decode($response->getBody(), true);
    }

    public function revokeToken($token)
    {
        $postKey = (version_compare(ClientInterface::VERSION, '6') === 1) ? 'form_params' : 'body';

        $response = $this->getHttpClient()->post($this->baseUrl . 'api/revoke-token/', [
            'headers' => ['Accept' => 'application/json'],
            $postKey => [
                'token' => $token,
                'client_secret' => $this->clientSecret,
                'client_id' => $this->clientId,
            ],
        ]);
        return json_decode($response->getBody(), true);
    }

    public function logout()
    {
        return redirect($this->baseUrl . 'sso/sair?client_id='.$this->clientId);
    }

    /**
     * {@inheritdoc}
     */
    protected function mapUserToObject(array $user)
    {
        $pessoa = $user['pessoa'];

        $nickname = explode(' ',$pessoa['nome']);
        return (new User)->setRaw($user)->map([
            'id' => $pessoa['cpf'],
            'cpf' => $pessoa['cpf'],
            'nickname' => $nickname[0].' '.end($nickname),
            'name' => $pessoa['nome'] ?? null,
            'email' => $pessoa['contato']['email'] ?? null,
            // 'avatar' => $pessoa['email'] ?? null
        ]);
    }

    protected function getbaseUrl($state)
    {
        return $this->baseUrl;
    }
}