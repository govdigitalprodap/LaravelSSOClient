<?php

namespace Prodap\LaravelSSO\Oauth2;

interface ProviderInterface
{
    /**
     * Redirect the user to the authentication page for the provider.
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function redirect();

    /**
     * Get the User instance for the authenticated user.
     *
     * @return \Prodap\LaravelSSO\Two\User
     */
    public function user();
}
