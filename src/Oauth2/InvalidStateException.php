<?php

namespace Prodap\LaravelSSO\Oauth2;

use InvalidArgumentException;

class InvalidStateException extends InvalidArgumentException
{
    //
}
