<?php

namespace Prodap\LaravelSSO\Contracts;

interface Provider
{
    /**
     * Redirect the user to the authentication page for the provider.
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function redirect();

    /**
     * Get the User instance for the authenticated user.
     *
     * @return \Prodap\LaravelSSO\Contracts\User
     */
    public function user();
}
