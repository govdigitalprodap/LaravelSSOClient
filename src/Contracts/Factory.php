<?php

namespace  Prodap\LaravelSSO\Contracts;

interface Factory
{
    /**
     * Get an OAuth provider implementation.
     *
     * @param  string  $driver
     * @return \Prodap\LaravelSSO\Contracts\Provider
     */
    public function driver($driver = null);
}
