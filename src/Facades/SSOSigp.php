<?php

namespace Prodap\LaravelSSO\Facades;

use Illuminate\Support\Facades\Facade;
use Prodap\LaravelSSO\Contracts\Factory;

/**
 * @method static \Prodap\LaravelSSO\Contracts\Provider driver(string $driver = null)
 * @see \Prodap\LaravelSSO\SocialiteManager
 */
class SSOSigp extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return Factory::class;
    }
}
